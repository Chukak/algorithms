#ifndef ALGORITHM_CH
#define ALGORITHM_CH

#include <random>
#include <type_traits>
#include <utility>
#include <iostream>
#include <string>
#include <sstream>
#include <list>
#include <functional>


namespace algo
{

namespace numbers
{

template<typename T>
constexpr bool is_owerflow(T a, T b) noexcept
{
	if (a == 0 && b == 0) {
		return false;
	}
	if ((a + b) == 0) {
		return ((a > 0 && b > 0) || (a < 0 && b < 0));
	}
	if (a > 0 && b > 0 && (a + b) > 0) {
		return ((a + b) < a);
	}
	return ((a > 0 && b > 0 && (a + b) < 0) || (a < 0 && b < 0 && (a + b) > 0));
}
}

namespace search
{

template<typename IT,
         typename T>
constexpr long linear_search(IT _begin, IT _end, T _key) noexcept
{
	typename std::iterator_traits<IT>::difference_type _diff = std::distance(_begin, _end);
	long _position = 0;
	while (_diff-- > 0) {
		if (*_begin++ == _key) {
			return _position;
		}
		++_position;
	}
	return -1;
}

}

namespace  random
{

template<typename T>
T random_range(const T& _min, const T& _max)
{
	T _result;
	std::random_device _device;
	std::mt19937 _mt(_device());
	if (std::is_same_v<int, T> || std::is_same_v<unsigned int, T>
	        || std::is_same_v<short, T> || std::is_same_v<unsigned short, T>
	        || std::is_same_v<long, T> || std::is_same_v<unsigned long, T>
	        || std::is_same_v<long long, T> || std::is_same_v<unsigned long long, T>) {
		std::uniform_int_distribution<> _dist(_min, _max);
		_result = _dist(_mt);
	} else if (std::is_same_v<float, T> || std::is_same_v<double, T>
	           || std::is_same_v<long double, T>) {
		std::uniform_real_distribution<> _dist(_min, _max);
		_result = _dist(_mt);
	} else {
		std::cout << "[DEBUG] Unknown type. Result will have integer type." << "\n" << std::flush;
		std::uniform_int_distribution<> _dist(_min, _max);
		_result = _dist(_mt);
	}
	return _result;
}

}

namespace math
{

namespace  points
{

struct point
{
	point(double x, double y) noexcept;
	~point() noexcept = default;
	point(point&&) noexcept = default;
	point(const point&) noexcept = default;
	point& operator=(point&&) noexcept = default;
	point& operator=(const point&) noexcept = default;

	constexpr inline double x() const noexcept
	{
		return _x;
	}
	constexpr inline double y() const noexcept
	{
		return _y;
	}
	constexpr inline void setX(double x) noexcept
	{
		_x = x;
	}
	constexpr inline void setY(double y) noexcept
	{
		_y = y;
	}
	constexpr void setXY(double x, double y) noexcept;
	bool operator==(const point& other) const noexcept;
	bool operator!=(const point& other) const noexcept;
	bool operator<(const point& other) const noexcept;
	bool operator>(const point& other) const noexcept;
	bool operator<=(const point& other) const noexcept;
	bool operator>=(const point& other) const noexcept;
private:
	double _x;
	double _y;
};

std::istream& operator>>(std::istream& stream, point& p);
std::ostream& operator<<(std::ostream& stream, const point& p);


template<typename X, typename Y,
         template <class ... > class ARRAY>
constexpr bool point_inside_polygon(X x, Y y, ARRAY<point>& array) noexcept
{
	bool result = false;
	auto at = [&array](size_t i) {
		return *std::next(std::begin(array), i);
	};
	auto size = static_cast<size_t>(std::size(array));
	for (size_t i = 0; i < size; ++i) {
		size_t j = (i + 1) % size;
		if (((at(i).y() < y && at(j).y() >= y) || (at(j).y() < y && at(i).y() >= y)) &&
		        (at(i).x() + (y - at(i).y()) / (at(j).y() - at(i).y()) * (at(j).x() - at(i).x()) < x)) {
			result = !result;
		}
	}
	return result;
}

template<typename X, typename Y,
         typename IT>
constexpr bool point_inside_polygon(X x, Y y, IT begin, IT end) noexcept
{
	bool result = false;
	auto first = *begin;
	for (size_t distance = std::abs(std::distance(begin, end)); begin != end; --distance, ++begin) {
		auto next = (distance == 1 ? first : *std::next(begin));
		if ((((*begin).y() < y && next.y() >= y) || (next.y() < y && (*begin).y() >= y)) &&
		        ((*begin).x() + (y - (*begin).y()) / (next.y() - (*begin).y()) * (next.x() - (*begin).x()) < x)) {
			result = !result;
		}
	}
	return result;
}

template<typename X, typename Y,
         typename IT,
         typename FUNCX, typename FUNCY>
constexpr bool point_inside_polygon(X x, Y y, IT begin, IT end, FUNCX funcX, FUNCY funcY) noexcept
{
	bool result = false;
	auto first = begin;
	for (size_t distance = std::abs(std::distance(begin, end)); begin != end; --distance, ++begin) {
		auto next = (distance == 1 ? first : begin);
		if (((((*begin).*funcY)() < y && ((*next).*funcY)() >= y) || (((*next).*funcY)() < y && ((*begin).*funcY)() >= y)) &&
		        (((*begin).*funcX)() + (y - ((*begin).*funcY)()) / (((*next).*funcY)() - ((*begin).*funcY)()) * (((*next).*funcX)() - ((*begin).*funcX)()) < x)) {
			result = !result;
		}
	}
	return result;
}


inline double distance(const point& p1, const point& p2) noexcept
{
	return std::sqrt((p1.x() - p2.x()) * (p1.x() - p2.x()) + (p1.y() - p2.y()) * (p1.y() - p2.y()));
}

template<typename T>
constexpr inline double distance(T x1, T y1, T x2, T y2) noexcept
{
	return std::sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
}


bool projection_point_on_straight(const point& p1, const point& p2, const point& p, point& result_point) noexcept;

template<typename T>
bool projection_point_on_straight(T const& x1, T const& y1, T const& x2, T const& y2, T const& px, T const& py, T& rX, T& rY)
{
	double first = (x1 * y2) - (x2 * y1) + (y1 * px) - (y2 * px) + (x2 * py) - (x1 * py);
	double second = (y2 - y1) * (y2 - y1) + (x1 - x2) * (x1 - x2);
	double line = first / second;
	point result(px + (y2 - y1) * line, py + (x1 - x2) * line);
	if (result.x() >= std::min(x1, x2) && result.x() <= std::max(x1, x2)) {
		if (distance(result.x(), result.y(), px, py) < std::min(distance(x1, y1, px, py), distance(x2, y2, px, py))) {
			rX = result.x(), rY = result.y();
			return true;
		}
	}
	rX = 0.0, rY = 0.0;
	return false;
}

template<typename T>
bool projection_point_on_straight(const T& x1, const T& y1, const T& x2, const T& y2, const T& px, const T& py, const T& rX, const T& rY) = delete;


point cross_straight_to_straight(const point& a, const point& b, const point& c, const point& d) noexcept;

template<typename T,
         typename POINT = std::pair<T, T>>
void cross_straight_to_straight(POINT && a, POINT && b, POINT && c, POINT && d, T& rX, T& rY) noexcept
{
	T A1 = a.second - b.second, A2 = c.second - d.second;
	T B1 = b.first - a.first, B2 = d.first - c.first;
	T C1 = a.first * b.second - b.first * a.second, C2 = c.first * d.second - d.first * c.second;
	rX = 0.0, rY = 0.0;
	if (!(std::fabs(A1 / A2 - B1 / B2) < 0.000001)) {
		rX = (C2 * B1 - C1 * B2) / (A1 * B2 - A2 * B1), rY = (C1 * A2 - C2 * A1) / (B2 * A1 - B1 * A2);
	}
}

template<typename T,
         typename POINT>
bool cross_straight_to_straight(const POINT& a, const POINT& b, const POINT& c, const POINT& d, const T& rX, const T& rY) = delete;
template<typename T,
         typename POINT = std::pair<T, T>>
void cross_straight_to_straight(POINT const && a, POINT const && b, POINT const && c, POINT const && d, T& rX, T& rY) = delete;


inline bool point_belong_to_straights(const point& p1, const point& p2, const point& p3, const point& p4, const point& p) noexcept
{
	return (!(std::fabs((p1.y() - p2.y()) / (p3.y() - p4.y()) - (p2.x() - p1.x()) / (p4.x() - p3.x())) < 0.000001)
	        &&
	        (p.x() >= std::min(p1.x(), p2.x()) && p.x() <= std::max(p1.x(), p2.x()) && p.x() >= std::min(p3.x(), p4.x()) && p.x() <= std::max(p3.x(), p4.x()))
	        &&
	        (p.y() >= std::min(p1.y(), p2.y()) && p.y() <= std::max(p1.y(), p2.y()) && p.y() >= std::min(p3.y(), p4.y()) && p.y() <= std::max(p3.y(), p4.y())));
}

template<typename T,
         typename POINT = std::pair<T, T>>
constexpr inline bool point_belong_to_straights(POINT const& p1, POINT const& p2, POINT const& p3, POINT const& p4, POINT const& p) noexcept
{
	return (!(std::fabs((p1.second - p2.second) / (p3.second - p4.second) - (p2.first - p1.first) / (p4.first - p3.first)) < 0.000001)
	        &&
	        (p.first >= std::min(p1.first, p2.xfirst) && p.first <= std::max(p1.first, p2.first) && p.x() >= std::min(p3.first, p4.first)
	         && p.first <= std::max(p3.first, p4.first))
	        &&
	        (p.second >= std::min(p1.second, p2.second) && p.second <= std::max(p1.second, p2.second) && p.second >= std::min(p3.second, p4.second)
	         && p.second <= std::max(p3.second, p4.second)));
}

bool cross_two_segments(const point& p1, const point& p2, const point& p3, const point& p4) noexcept;

template<typename T,
         typename POINT = std::pair<T, T>>
constexpr bool cross_two_segments(POINT && p1, POINT && p2, POINT && p3, POINT && p4) noexcept
{
	T a1 = p2.second - p1.second;
	T b1 = p1.first - p2.first;
	T c1 = (p2.first * p1.second) - (p1.first * p2.second);

	T d1 = (a1 * p3.first) + (b1 * p3.second) + c1;
	T d2 = (a1 * p4.first) + (b1 * p4.second) + c1;

	if ((d1 > 0 && d2 > 0) || (d1 < 0 && d2 < 0)) {
		return false;
	}

	T a2 = p4.second - p3.second;
	T b2 = p3.first - p4.first;
	T c2 = (p4.first * p3.second) - (p3.first * p4.second);

	d1 = (a2 * p1.first) + (b2 * p1.second) + c2;
	d2 = (a2 * p2.first) + (b2 * p2.second) + c2;

	if ((d1 > 0 && d2 > 0) || (d1 < 0 && d2 < 0)) {
		return false;
	}
	return !((a1 * b2) - (a2 * b1) < 0.000001);
}

template<typename T,
         typename POINT>
constexpr bool cross_two_segments(POINT const& p1, POINT const& p2, POINT const& p3, POINT const& p4) = delete;
template<typename T,
         typename POINT = std::pair<T, T>>
constexpr bool cross_two_segments(POINT const && p1, POINT const && p2, POINT const && p3, POINT const && p4) = delete;

int cross_segment_to_circle(const point& p1, const point& p2, const point& center, double radius, point& intersection_point1,
                            point& intersection_point2) noexcept;

template<typename T,
         typename POINT = std::pair<T, T>>
constexpr int cross_segment_to_circle(POINT && p1, POINT && p2, POINT && center, const double radius, T& x1, T& y1, T& x2, T& y2) noexcept
{
	int count = 0;
	if (radius > distance(p1.first, p1.second, center.first, center.second)) {
		x1 = p1.first, y1 = p1.second;
		++count;
		if (std::fabs(p1.first - p2.first) < 0.000001) {
			return count;
		}
	}
	if (radius > distance(p2.first, p2.second, center.first, center.second)) {
		(!count ? (x1 = p2.first, y1 = p2.second) : (x2 = p2.first, y2 = p2.second));
		++count;
	}
	if (!count) {
		T a = std::pow(p2.first - p1.first, 2) + std::pow(p2.second - p1.second, 2);
		T b = (p2.first - p1.first) * (center.first - p1.first) + (p2.second - p1.second) * (center.second - p1.second);
		T c = std::pow(center.first - p1.first, 2) + std::pow(center.second - p1.second, 2) - std::pow(radius, 2);

		T d = std::pow(b / a, 2) - c / a;
		if (!(d < 0)) {
			auto set_point = [&p1, &p2](T & x, T & y, T scaling) {
				x = p1.first - (p2.first - p1.first) * scaling, y = p1.second - (p2.second - p1.second) * scaling;
			};
			set_point(x1, y1, -(b / a) + std::sqrt(d));
			++count;
			if (d > 0) {
				set_point(x2, y2, -(b / a) - std::sqrt(d));
				++count;
			}
		}
	}
	return count;
}

template<typename T,
         typename POINT = std::pair<T, T>>
constexpr int cross_segment_to_circle(POINT const& p1, POINT const& p2, POINT const& center, const double radius, const T& x1, const T& y1, const T& x2,
                                      const T& y2) = delete;
template<typename T,
         typename POINT = std::pair<T, T>>
constexpr int cross_segment_to_circle(POINT const && p1, POINT const && p2, POINT const && center, const double radius, const T& x1, const T& y1, const T& x2,
                                      const T& y2) = delete;
}
}

namespace strings
{

std::string add_line_breaks(const std::string& original, size_t length, bool save_spaces = false) noexcept;

}

namespace notice
{

template<typename ... Args>
class signal
{
	using delegate = std::function<void(Args ...)>;

public:
	signal() noexcept = default;
	~signal() noexcept = default;
	signal(signal&&) noexcept = default;
	signal(const signal&) = default;
	signal& operator=(signal&&) noexcept = default;
	signal& operator=(const signal&) = default;

	void connect(const delegate& fn) noexcept;
	template<typename Class, typename FUNC>
	void connect(Class * instance, FUNC fn) noexcept;
	void disconnect(const delegate& fn) noexcept;
	template<typename Class, typename FUNC>
	void disconnect(Class * instance, FUNC fn) noexcept;
	void disconnect_all() noexcept;
	typename std::enable_if_t<sizeof ... (Args) == 0> emit() const noexcept;
	typename std::enable_if_t < sizeof ... (Args) != 0 > emit(const Args& ... args) const noexcept;
	typename std::enable_if_t < sizeof ... (Args) != 0 > emit(Args&& ... args) const noexcept;

private:
	void emit_p(const Args& ... args) const noexcept;
	decltype(auto) find_member(const std::uintptr_t& address) const noexcept;
	const std::string get_name(const void * p) const noexcept;

private:
	std::list<std::pair<std::uintptr_t, delegate>> _memslots;
	std::list<delegate> _slots;
};

template<typename ... Args> decltype(auto) signal<Args ...>::find_member(const std::uintptr_t& address) const noexcept
{
	for (auto it = _memslots.begin(); it != _memslots.end(); ++it) {
		if (((*it).first) == address) {
			return it;
		}
	}
	return _memslots.end();
}

template<typename ... Args>
const std::string signal<Args ...>::get_name(const void * p) const noexcept
{
	std::stringstream stream;
	stream << p;
	return stream.str();
}

template<typename ... Args>
void signal<Args ...>::connect(const delegate& fn) noexcept
{
	_slots.push_back(fn);
}

template<typename ... Args>
template<typename Class, typename FUNC>
void signal<Args ...>::connect(Class * instance, FUNC fn) noexcept
{
	const std::string class_name = get_name(static_cast<const void *>(instance));
	// todo: need to replace reinterpret_cast to better decision
	std::uintptr_t address = reinterpret_cast<std::uintptr_t&>(fn);
	if (find_member(address) == _memslots.end()) {
		_memslots.push_back(std::make_pair(address, [instance, fn](Args ... args) {
			(instance->*fn)(args ...);
		}));
	}
}

template<typename ... Args>
void signal<Args ...>::disconnect(const delegate& fn) noexcept
{
	_slots.remove(fn);
}

template<typename ... Args>
template<typename Class, typename FUNC>
void signal<Args ...>::disconnect(Class * instance, FUNC fn) noexcept
{
	const std::string class_name = get_name(static_cast<const void *>(instance));
	// todo: need to replace reinterpret_cast to better decision
	auto it = find_member(reinterpret_cast<std::uintptr_t&>(fn));
	if (it != _memslots.end()) {
		_memslots.erase(it);
	}
}

template<typename ... Args>
void signal<Args ...>::disconnect_all() noexcept
{
	_slots.clear();
	_memslots.clear();
}

template<typename ... Args>
void signal<Args ...>::emit_p(const Args& ... args) const noexcept
{
	bool end_it_funcs = false, end_it_memfuncs = false;
	for (auto it_funcs = _slots.begin(), it_memfuncs = _memslots.begin(); !end_it_funcs || !end_it_memfuncs;) {
		if (!(end_it_funcs = (end_it_funcs || it_funcs == _slots.end()))) {
			(*it_funcs++)(args ...);
		}
		if (!(end_it_memfuncs = (end_it_memfuncs || it_memfuncs == _memslots.end()))) {
			(*it_memfuncs++).second(args ...);
		}
	}
}

template<typename ... Args>
typename std::enable_if_t<sizeof ... (Args) == 0> signal<Args ...>::emit() const noexcept
{
	emit_p();
}

template<typename ... Args>
typename std::enable_if_t < sizeof ... (Args) != 0 > signal<Args ...>::emit(const Args& ... args) const noexcept
{
	emit_p(args ...);
}

template<typename ... Args>
typename std::enable_if_t < sizeof ... (Args) != 0 > signal<Args ...>::emit(Args&& ... args) const noexcept
{
	emit_p(static_cast<const Args&>(args) ...);
}

}

}

#endif
