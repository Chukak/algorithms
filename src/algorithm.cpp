#include "algorithm.h"
#include <regex>

namespace algo
{

namespace math
{

namespace points
{

point::point(double x, double y) noexcept :
	_x(x),
	_y(y)
{
}

constexpr void point::setXY(double x, double y) noexcept
{
	_x = x;
	_y = y;
}

bool point::operator==(const point& other) const noexcept
{
	return std::fabs(_x - other._x) < 0.000001 && std::fabs(_y - other._y) < 0.000001;
}
bool point::operator!=(const point& other) const noexcept
{
	return !(std::fabs(_x - other._x) < 0.000001 && std::fabs(_y - other._y) < 0.000001);
}
bool point::operator<(const point& other) const noexcept
{
	return _x < other._x || _y < other._y;
}
bool point::operator>(const point& other) const noexcept
{
	return _x > other._x || _y > other._y;
}
bool point::operator<=(const point& other) const noexcept
{
	return _x <= other._x || _y <= other._y;
}
bool point::operator>=(const point& other) const noexcept
{
	return _x >= other._x || _y >= other._y;
}

std::istream& operator>>(std::istream& stream, point& p)
{
	double x, y;
	stream >> x >> y;
	p.setXY(x, y);
	return stream;
}

std::ostream& operator<<(std::ostream& stream, const point& p)
{
	stream << " X: " << p.x() << " Y: " << p.y();
	return stream;
}


bool projection_point_on_straight(const point& p1, const point& p2, const point& p, point& result_point) noexcept
{
	double first = (p1.x() * p2.y()) - (p2.x() * p1.y()) + (p1.y() * p.x()) - (p2.y() * p.x()) + (p2.x() * p.y()) - (p1.x() * p.y());
	double second = (p2.y() - p1.y()) * (p2.y() - p1.y()) + (p1.x() - p2.x()) * (p1.x() - p2.x());
	double line = first / second;
	point result(p.x() + (p2.y() - p1.y()) * line, p.y() + (p1.x() - p2.x()) * line);
	if (result.x() >= std::min(p1.x(), p2.x()) && result.x() <= std::max(p1.x(), p2.x())) {
		if (distance(result, p) < std::min(distance(p1, p), distance(p2, p))) {
			result_point.setXY(result.x(), result.y());
			return true;
		}
	}
	result_point.setXY(0.0, 0.0);
	return false;
}


point cross_straight_to_straight(const point& a, const point& b, const point& c, const point& d) noexcept
{
	double A1 = a.y() - b.y(), A2 = c.y() - d.y();
	double B1 = b.x() - a.x(), B2 = d.x() - c.x();
	double C1 = a.x() * b.y() - b.x() * a.y(), C2 = c.x() * d.y() - d.x() * c.y();
	if (std::fabs(A1 / A2 - B1 / B2) < 0.000001) {
		return {0.0, 0.0};
	}
	return {(C2 * B1 - C1 * B2) / (A1 * B2 - A2 * B1), (C1 * A2 - C2 * A1) / (B2 * A1 - B1 * A2)};
}

bool cross_two_segments(const point& p1, const point& p2, const point& p3, const point& p4) noexcept
{
	double a1 = p2.y() - p1.y();
	double b1 = p1.x() - p2.x();
	double c1 = (p2.x() * p1.y()) - (p1.x() * p2.y());

	double d1 = (a1 * p3.x()) + (b1 * p3.y()) + c1;
	double d2 = (a1 * p4.x()) + (b1 * p4.y()) + c1;

	if ((d1 > 0 && d2 > 0) || (d1 < 0 && d2 < 0)) {
		return false;
	}

	double a2 = p4.y() - p3.y();
	double b2 = p3.x() - p4.x();
	double c2 = (p4.x() * p3.y()) - (p3.x() * p4.y());

	d1 = (a2 * p1.x()) + (b2 * p1.y()) + c2;
	d2 = (a2 * p2.x()) + (b2 * p2.y()) + c2;

	if ((d1 > 0 && d2 > 0) || (d1 < 0 && d2 < 0)) {
		return false;
	}
	return !((a1 * b2) - (a2 * b1) < 0.000001);
}

int cross_segment_to_circle(const point& p1, const point& p2, const point& center, const double radius, point& intersection_point1,
                            point& intersection_point2) noexcept
{
	int count = 0;
	if (radius > distance(p1, center)) {
		intersection_point1.setXY(p1.x(), p1.y());
		++count;
		if (std::fabs(p1.x() - p2.x()) < 0.000001) {
			return count;
		}
	}
	if (radius > distance(p2, center)) {
		(!count ? intersection_point1.setXY(p2.x(), p2.y()) : intersection_point2.setXY(p2.x(), p2.y()));
		++count;
	}
	if (!count) {
		double a = std::pow(p2.x() - p1.x(), 2) + std::pow(p2.y() - p1.y(), 2);
		double b = (p2.x() - p1.x()) * (center.x() - p1.x()) + (p2.y() - p1.y()) * (center.y() - p1.y());
		double c = std::pow(center.x() - p1.x(), 2) + std::pow(center.y() - p1.y(), 2) - std::pow(radius, 2);

		double d = std::pow(b / a, 2) - c / a;
		if (!(d < 0)) {
			auto set_point = [&p1, &p2](point & result, double scaling) {
				result.setXY(p1.x() - (p2.x() - p1.x()) * scaling, p1.y() - (p2.y() - p1.y()) * scaling);
			};
			set_point(intersection_point1, -(b / a) + std::sqrt(d));
			++count;
			if (d > 0) {
				set_point(intersection_point2, -(b / a) - std::sqrt(d));
				++count;
			}
		}
	}
	return count;
}
}

}

namespace strings
{

std::string add_line_breaks(const std::string& original, const size_t length, const bool save_spaces) noexcept
{
	if (original.length() <= length) {
		return original;
	}

	std::string result(original);
	std::regex rx("[\\s\\n\\r\\t][^\\s\\n\\r\\t]*$");
	size_t index_first = 0;
	size_t size = 0;
	while (size < original.length()) {
		bool found_break = false;
		do {
			std::regex rx_break("\\n(?=[^\\n]{1,1})");
			std::smatch match;
			std::string part_break = result.substr(index_first, length);
			if ((found_break = std::regex_search(part_break.cbegin(), part_break.cend(), match, rx_break))) {
				auto pos = static_cast<size_t>(match.position(0));
				index_first += pos + 1;
				size += pos + 1;
			}
		} while (found_break && size < original.length());

		std::string part = result.substr(index_first, length);
		if (part.length() >= length) {
			std::smatch match;
			if (!std::regex_search(part.cbegin(), part.cend(), match, rx)) {
				std::regex rx_part("[\\s\\n\\r\\t]");
				size_t length_part_two = length;
				std::string part_two;
				bool found = false;
				do {
					if (index_first + length_part_two > result.size()) {
						return result;
					}
					length_part_two += length;
					part_two = result.substr(index_first, (index_first + length_part_two < result.size()
					                                       ? length_part_two
					                                       : std::string::npos));
					found = std::regex_search(part_two.cbegin(), part_two.cend(), match, rx_part);
				} while (!found);
			}
			auto pos = static_cast<size_t>(match.position(0));
			size_t jump = pos + 1;
			if (save_spaces) {
				++jump;
				result.insert(index_first + pos, "\n");
			} else {
				result.replace(index_first + pos, 1, "\n");
			}
			index_first += jump;
			size += jump;
		} else {
			size += part.length();
		}
	}
	return result;
}

}

}
