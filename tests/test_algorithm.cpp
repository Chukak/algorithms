#include <catch.hpp>
#include "algorithm.h"
#include <vector>
#include <list>
#include <array>
#include <limits>

TEST_CASE("random_range")
{
	SECTION("random_range<int>")
	{
		using namespace algo::random;

		int a = -58, b = 754;
		for (int r = 0; r < 1000; ++r) {
			int result = random_range<int>(a, b);
			REQUIRE((result >= a && result <= b));
			REQUIRE(std::is_same_v<int, std::decay_t<decltype (result)>>);
		}
	}
	SECTION("random_range<double>")
	{
		using namespace algo::random;

		double a = -22.4, b = 152.6;
		for (int r = 0; r < 1000; ++r) {
			auto result = random_range<double>(a, b);
			REQUIRE((result >= a && result <= b));
			REQUIRE(std::is_same_v<double, std::decay_t<decltype (result)>>);
		}
	}
	SECTION("random_range<long>")
	{
		using namespace algo::random;

		long a = 46534, b = 67934;
		for (int r = 0; r < 1000; ++r) {
			auto result = random_range<long>(a, b);
			REQUIRE((result >= a && result <= b));
			REQUIRE(std::is_same_v<long, std::decay_t<decltype (result)>>);
		}
	}
	SECTION("random_range<long long>")
	{
		using namespace algo::random;

		long long a = -65239, b = 9876345;
		for (int r = 0; r < 1000; ++r) {
			auto result = random_range<long long>(a, b);
			REQUIRE((result >= a && result <= b));
			REQUIRE(std::is_same_v<long long, std::decay_t<decltype (result)>>);
		}
	}
	SECTION("random_range<short>")
	{
		using namespace algo::random;

		short a = -126, b = 126;
		for (int r = 0; r < 1000; ++r) {
			auto result = random_range<short>(a, b);
			REQUIRE((result >= a && result <= b));
			REQUIRE(std::is_same_v<short, std::decay_t<decltype (result)>>);
		}
	}
	SECTION("random_range<float>")
	{
		using namespace algo::random;

		float a = -0.78f, b = 98.23f;
		for (int r = 0; r < 1000; ++r) {
			auto result = random_range<float>(a, b);
			REQUIRE((result >= a && result <= b));
			REQUIRE(std::is_same_v<float, std::decay_t<decltype (result)>>);
		}
	}
}

TEST_CASE("linear_search")
{
	SECTION("linear_search")
	{
		using namespace algo::search;
		using namespace algo::random;

		std::vector<int> v, v2;
		for (int i = 0; i < 1000; ++i) {
			int result = random_range<int>(-10000, 10000);
			v.push_back(result);
			v2.push_back(result);
		}
		REQUIRE(v.size() == 1000);
		REQUIRE(v2.size() == 1000);
		for (long i = static_cast<long>(v2.size()) - 1; i >= 0; --i) {
			long pos = linear_search(v.begin(), v.end(), v2.at(static_cast<size_t>(i)));
			REQUIRE(pos != -1);
			REQUIRE(0 <= pos);
			REQUIRE(pos < 1000);
		}
		REQUIRE(linear_search(v.begin(), v.end(), -12560) == -1);
		REQUIRE(linear_search(v.begin(), v.end(), 16983) == -1);
	}
}

TEST_CASE("is_owerflow")
{
	SECTION("is_overflow(int, int)")
	{
		using namespace algo::numbers;

		int a = std::numeric_limits<int>::max() - 10;
		int b = 11;
		REQUIRE(is_owerflow(a, b));
	}
	SECTION("is_overflow(long, long)")
	{
		using namespace algo::numbers;

		long a = std::numeric_limits<long>::max() - 1001;
		long b = 1002;
		REQUIRE(is_owerflow(a, b));
	}
	SECTION("is_overflow(short, short)")
	{
		using namespace algo::numbers;

		short a = std::numeric_limits<short>::min() + 100;
		short b = -2000;
		// compiler extends short to int
		REQUIRE(!is_owerflow(a, b));
	}
	SECTION("is_overflow(unsigned, unsigned)")
	{
		using namespace algo::numbers;

		unsigned a = std::numeric_limits<unsigned>::max();
		unsigned b = 2;
		REQUIRE(is_owerflow(a, b));
	}
}

TEST_CASE("point_inside_polygon")
{
	SECTION("point_inside_poygon(X, Y, std::vector<point>)")
	{
		using namespace algo;
		using namespace math::points;

		point p = {3, 6};
		std::vector<point> points = {{1, 2}, {4, 1}, {6, 2}, {6, 9}, {4, 8}, {1, 9}};
		REQUIRE(point_inside_polygon(p.x(), p.y(), points));

		point p2 = {1, 1};
		std::vector<point> points2 = {{1, 2}, {4, 1}, {6, 2}, {6, 9}, {4, 8}, {1, 9}};
		REQUIRE_FALSE(point_inside_polygon(p2.x(), p2.y(), points2));
	}
	SECTION("point_inside_polygon(X, Y, IT std::begin(std::vector<point>), IT std::end(std::vector<point>))")
	{
		using namespace algo;
		using namespace math::points;

		point p = {3, 6};
		std::vector<point> points = {{1, 2}, {4, 1}, {6, 2}, {6, 9}, {4, 8}, {1, 9}};
		REQUIRE(point_inside_polygon(p.x(), p.y(), points.begin(), points.end()));

		point p2 = {1, 1};
		std::vector<point> points2 = {{1, 2}, {4, 1}, {6, 2}, {6, 9}, {4, 8}, {1, 9}};
		REQUIRE_FALSE(point_inside_polygon(p2.x(), p2.y(), points2.begin(), points2.end()));
	}
	SECTION("point_inside_polygon(X, Y, IT std::begin(std::list<point>), IT std::end(std::list<point>))")
	{
		using namespace algo;
		using namespace math::points;

		point p = {3, 6};
		std::list<point> points = {{1, 2}, {4, 1}, {6, 2}, {6, 9}, {4, 8}, {1, 9}};
		REQUIRE(point_inside_polygon(p.x(), p.y(), points.begin(), points.end()));

		point p2 = {1, 1};
		std::list<point> points2 = {{1, 2}, {4, 1}, {6, 2}, {6, 9}, {4, 8}, {1, 9}};
		REQUIRE_FALSE(point_inside_polygon(p2.x(), p2.y(), points2.begin(), points2.end()));
	}
	SECTION("point_inside_poygon(X, Y, IT std::begin(std::array<point, 6>), IT std::end(std::array<point, 7>)")
	{
		using namespace algo;
		using namespace math::points;

		point p = {3, 6};
		point a = {1, 2}, b = {4, 1}, c = {6, 2}, d = {6, 9}, e = {4, 8}, f = {1, 9};
		std::array<point, 6> points = {a, b, c, d, e, f};
		REQUIRE(point_inside_polygon(p.x(), p.y(), points.begin(), points.end()));

		point p2 = {1, 1};
		std::array<point, 6> points2 = {a, b, c, d, e, f};
		REQUIRE_FALSE(point_inside_polygon(p2.x(), p2.y(), points2.begin(), points2.end()));
	}
	SECTION("point_inside_polygon(X, Y, IT std::begin(std::vector<custom_point>), IT std::end(std::vector<custom_point>), FUNCX, FUNCY")
	{
		class custom_point
		{
		public:
			custom_point(double x, double y) : _x(x), _y(y) {}
			inline double getX() const noexcept {
				return _x;
			}
			inline double getY() const noexcept {
				return _y;
			}
		private:
			double _x, _y;
		};

		using namespace algo;
		using namespace math::points;

		custom_point p = {3, 6};
		std::vector<custom_point> points = {{1, 2}, {4, 1}, {6, 2}, {6, 9}, {4, 8}, {1, 9}};
		REQUIRE(point_inside_polygon(p.getX(), p.getY(), points.begin(), points.end(), &custom_point::getX, &custom_point::getY));

		custom_point p2 = {1, 1};
		std::vector<custom_point> points2 = {{1, 2}, {4, 1}, {6, 2}, {6, 9}, {4, 8}, {1, 9}};
		REQUIRE_FALSE(point_inside_polygon(p2.getX(), p2.getY(), points2.begin(), points2.end(), &custom_point::getX, &custom_point::getY));
	}
	SECTION("point_inside_polygon(X, Y, IT std::begin(std::list<custom_point>), IT std::end(std::list<custom_point>), FUNCX, FUNCY")
	{
		class custom_point
		{
		public:
			custom_point(double x, double y) : _x(x), _y(y) {}
			inline double getX() const noexcept {
				return _x;
			}
			inline double getY() const noexcept {
				return _y;
			}
		private:
			double _x, _y;
		};

		using namespace algo;
		using namespace math::points;

		custom_point p = {3, 6};
		std::list<custom_point> points = {{1, 2}, {4, 1}, {6, 2}, {6, 9}, {4, 8}, {1, 9}};
		REQUIRE(point_inside_polygon(p.getX(), p.getY(), points.begin(), points.end(), &custom_point::getX, &custom_point::getY));

		custom_point p2 = {1, 1};
		std::list<custom_point> points2 = {{1, 2}, {4, 1}, {6, 2}, {6, 9}, {4, 8}, {1, 9}};
		REQUIRE_FALSE(point_inside_polygon(p2.getX(), p2.getY(), points2.begin(), points2.end(), &custom_point::getX, &custom_point::getY));
	}
	SECTION("point_inside_polygon(X, Y, IT std::begin(std::array<custom_point, 6>), IT std::end(std::array<custom_point, 6>), FUNCX, FUNCY")
	{
		class custom_point
		{
		public:
			custom_point(double x, double y) : _x(x), _y(y) {}
			inline double getX() const noexcept {
				return _x;
			}
			inline double getY() const noexcept {
				return _y;
			}
		private:
			double _x, _y;
		};

		using namespace algo;
		using namespace math::points;

		custom_point p = {3, 6};
		custom_point a = {1, 2}, b = {4, 1}, c = {6, 2}, d = {6, 9}, e = {4, 8}, f = {1, 9};
		std::array<custom_point, 6> points = {a, b, c, d, e, f};
		REQUIRE(point_inside_polygon(p.getX(), p.getY(), points.begin(), points.end(), &custom_point::getX, &custom_point::getY));

		custom_point p2 = {1, 1};
		std::array<custom_point, 6> points2 = {a, b, c, d, e, f};
		REQUIRE_FALSE(point_inside_polygon(p2.getX(), p2.getY(), points2.begin(), points2.end(), &custom_point::getX, &custom_point::getY));
	}
}

TEST_CASE("math::points::class point")
{
	SECTION("operators")
	{
		using namespace algo;
		using namespace math::points;

		point p1(1, 2);
		point p2(2, 4);

		REQUIRE_FALSE(p1 == p2);
		REQUIRE(p1 != p2);
		REQUIRE_FALSE((p1 > p2 && p1 >= p2));
		REQUIRE((p1 < p2 && p1 <= p2));

		point p3(3, 5);
		point p4 = p3;

		REQUIRE_FALSE(p3 != p4);
		REQUIRE(p3 == p4);
		REQUIRE_FALSE((p3 > p4 && p3 < p4));
		REQUIRE((p3 >= p4 && p4 <= p4));
	}
}

TEST_CASE("math::points functions")
{
	SECTION("distance")
	{
		using namespace algo;
		using namespace math::points;

		point p1(1, 2);
		point p2(2, 4);
		point p3(4, 7);
		point p4(7, 1);

		REQUIRE((distance(p1, p2) - 2.2360) < 0.0001);
		REQUIRE((distance(p3, p4) - 6.7082) < 0.0001);
	}
	SECTION("distance<T>")
	{
		using namespace algo;
		using namespace math::points;

		double x1 = 1, y1 = 2, x2 = 2, y2 = 4;
		REQUIRE((distance(x1, y1, x2, y2) - 2.2360) < 0.0001);
		int ix1 = 4, iy1 = 7, ix2 = 7, iy2 = 1;
		REQUIRE((distance(ix1, iy1, ix2, iy2) - 6.7082) < 0.0001);
	}
	SECTION("projection_point_on_straight(point, ...)")
	{
		using namespace algo;
		using namespace math::points;

		point p1(53.674, 48.6453);
		point p2(51.3423, 50.3532);
		point pp(52.12, 49.67);

		point result(0.0, 0.0);
		REQUIRE(projection_point_on_straight(p1, p2, pp, result));
		REQUIRE((result.x() - 52.1741) < 0.0001);
		REQUIRE((result.y() - 49.7439) < 0.0001);
	}
	SECTION("projection_point_on_straight(x1, y1, ...)")
	{
		using namespace algo;
		using namespace math::points;

		double x1 = 53.674, y1 = 48.6453;
		double x2 = 51.3423, y2 = 50.3532;
		double px = 52.12, py = 49.67;

		double resultX, resultY;
		REQUIRE(projection_point_on_straight(x1, y1, x2, y2, px, py, resultX, resultY));
		REQUIRE((resultX - 52.1741) < 0.0001);
		REQUIRE((resultY - 49.7439) < 0.0001);
	}
	SECTION("cross_straight_to_straight(point, ...)")
	{
		using namespace algo;
		using namespace math::points;

		point p1(1.15, 7.45), p2(5.10, 1.25);
		point p3(1.15, 1.10), p4(7.00, 5.15);

		point result1 = cross_straight_to_straight(p1, p2, p3, p4);
		REQUIRE((result1.x() - 3.9573) < 0.0001);
		REQUIRE((result1.y() - 3.0435) < 0.0001);

		point p5(1.15, 7.45), p6(5.10, 1.25);
		point p7(3.15, 7.45), p8(7.10, 1.25);

		point result2 = cross_straight_to_straight(p5, p6, p7, p8);
		REQUIRE((result2.x() - 0) < 0.0001);
		REQUIRE((result2.y() - 0) < 0.0001);

		point p9(4.50, 1.05), p10(2.40, 2.10);
		point p11(4.90, 5.45), p12(3.10, 1.00);

		point result3 = cross_straight_to_straight(p9, p10, p11, p12);
		REQUIRE((result3.x() - 3.3523) < 0.0001);
		REQUIRE((result3.y() - 1.6238) < 0.0001);
	}
	SECTION("cross_straight_to_straight(POINT, POINT, ...)")
	{
		using namespace algo;
		using namespace math::points;

		double x1 = 1.15, y1 = 7.45, x2 = 5.10, y2 = 1.25;
		double x3 = 1.15, y3 = 1.10, x4 = 7.00, y4 = 5.15;

		double resultX, resultY;
		cross_straight_to_straight({x1, y1}, {x2, y2}, {x3, y3}, {x4, y4}, resultX, resultY);
		REQUIRE((resultX - 3.9573) < 0.0001);
		REQUIRE((resultY - 3.0435) < 0.0001);

		double x5 = 1.15, y5 = 7.45, x6 = 5.10, y6 = 1.25;
		double x7 = 3.15, y7 = 7.45, x8 = 7.10, y8 = 1.25;

		double result2X, result2Y;
		cross_straight_to_straight({x5, y5}, {x6, y6}, {x7, y7}, {x8, y8}, result2X, result2Y);
		REQUIRE((result2X - 0) < 0.0001);
		REQUIRE((result2Y - 0) < 0.0001);

		double x9 = 4.50, y9 = 1.05, x10 = 2.40, y10 = 2.10;
		double x11 = 4.90, y11 = 5.45, x12 = 3.10, y12 = 1.00;

		double result3X, result3Y;
		cross_straight_to_straight({x9, y9}, {x10, y10}, {x11, y11}, {x12, y12}, result3X, result3Y);
		REQUIRE((result3X - 3.3523) < 0.0001);
		REQUIRE((result3Y - 1.6238) < 0.0001);
	}
	SECTION("common_point(point, ...)")
	{
		using namespace algo;
		using namespace math::points;

		point p1(1.15, 7.45), p2(5.10, 1.25);
		point p3(1.15, 1.10), p4(7.00, 5.15);
		point p(3.9573, 3.0435);

		REQUIRE(point_belong_to_straights(p1, p2, p3, p4, p));

		point p5(1.15, 7.45), p6(5.10, 1.25);
		point p7(3.15, 7.45), p8(7.10, 1.25);

		REQUIRE_FALSE(point_belong_to_straights(p5, p6, p7, p8, p));
	}
	SECTION("common_point(POINT, ...)")
	{
		using namespace algo;
		using namespace math::points;

		double x1 = 1.15, y1 = 7.45, x2 = 5.10, y2 = 1.25;
		double x3 = 1.15, y3 = 1.10, x4 = 7.00, y4 = 5.15;
		double px = 3.9573, py = 3.0435;
		REQUIRE(point_belong_to_straights({x1, y1}, {x2, y2}, {x3, y3}, {x4, y4}, {px, py}));

		double x5 = 1.15, y5 = 7.45, x6 = 5.10, y6 = 1.25;
		double x7 = 3.15, y7 = 7.45, x8 = 7.10, y8 = 1.25;

		REQUIRE_FALSE(point_belong_to_straights({x5, y5}, {x6, y6}, {x7, y7}, {x8, y8}, {px, py}));
	}
	SECTION("cross_two_segments(point, ...)")
	{
		using namespace algo;
		using namespace math::points;

		point p1(1.15, 7.45), p2(5.10, 1.25);
		point p3(1.15, 1.10), p4(5.00, 7.15);

		REQUIRE(cross_two_segments(p1, p2, p3, p4));

		point p5(7.15, 7.45), p6(7.10, 7.25);
		point p7(1.15, 1.10), p8(1.00, 7.15);

		REQUIRE_FALSE(cross_two_segments(p5, p6, p7, p8));
	}
	SECTION("cross_two_segments(POINT, ...)")
	{
		using namespace algo;
		using namespace math::points;

		double x1 = 1.15, y1 = 7.45, x2 = 5.10, y2 = 1.25;
		double x3 = 1.15, y3 = 1.10, x4 = 5.00, y4 = 7.15;

		REQUIRE(cross_two_segments({x1, y1}, {x2, y2}, {x3, y3}, {x4, y4}));

		double x5 = 7.15, y5 = 7.45, x6 = 7.10, y6 = 7.25;
		double x7 = 1.15, y7 = 1.10, x8 = 1.00, y8 = 7.15;

		REQUIRE_FALSE(cross_two_segments({x5, y5}, {x6, y6}, {x7, y7}, {x8, y8}));
	}
	SECTION("cross_segment_to_circle(point, ...)")
	{
		using namespace algo;
		using namespace math::points;

		point p1(1.15, 7.45), p2(7.10, 1.25);
		point center1(4.15, 4.10);
		double radius1 = 4.1;

		point intersection_point1(0.0, 0.0), intersection_point2(0.0, 0.0);
		REQUIRE(cross_segment_to_circle(p1, p2, center1, radius1, intersection_point1, intersection_point2) == 2);
		REQUIRE((intersection_point1.x() - 1.4250) < 0.0001);
		REQUIRE((intersection_point1.y() - 7.1634) < 0.0001);
		REQUIRE((intersection_point2.x() - 7.0987) < 0.0001);
		REQUIRE((intersection_point2.y() - 1.2513) < 0.0001);

		point p3(7.15, 7.45), p4(7.10, 3.25);
		point center2(1.15, 1.10);
		double radius2 = 5;

		point intersection_point3(0.0, 0.0), intersection_point4(0.0, 0.0);
		REQUIRE(cross_segment_to_circle(p3, p4, center2, radius2, intersection_point3, intersection_point4) == 0);
		REQUIRE((intersection_point3.x() - 0.0) < 0.0001);
		REQUIRE((intersection_point3.y() - 0.0) < 0.0001);
		REQUIRE((intersection_point4.x() - 0.0) < 0.0001);
		REQUIRE((intersection_point4.y() - 0.0) < 0.0001);

		point p5(5.15, 1.15), p6(7.10, 6.25);
		point center3(6.15, 6.10);
		double radius3 = 3;

		point intersection_point5(0.0, 0.0), intersection_point6(0.0, 0.0);
		REQUIRE(cross_segment_to_circle(p5, p6, center3, radius3, intersection_point5, intersection_point6) == 1);
		REQUIRE((intersection_point5.x() - 7.10) < 0.0001);
		REQUIRE((intersection_point5.y() - 6.25) < 0.0001);
		REQUIRE((intersection_point6.x() - 0.0) < 0.0001);
		REQUIRE((intersection_point6.y() - 0.0) < 0.0001);
	}
	SECTION("cross_segment_to_circle(POINT, ...")
	{
		using namespace algo;
		using namespace math::points;

		double x1 = 1.15, y1 = 7.45, x2 = 7.10, y2 = 1.25;
		double centerX = 4.15, centerY = 4.10;
		double radius1 = 4.1;

		double ipx1 = 0.0, ipy1 = 0.0, ipx2 = 0.0, ipy2 = 0.0;
		REQUIRE(cross_segment_to_circle({x1, y1}, {x2, y2}, {centerX, centerY}, radius1, ipx1, ipy1, ipx2, ipy2) == 2);
		REQUIRE((ipx1 - 1.4250) < 0.0001);
		REQUIRE((ipy1 - 7.1634) < 0.0001);
		REQUIRE((ipx2 - 7.0987) < 0.0001);
		REQUIRE((ipy2 - 1.2513) < 0.0001);

		double x3 = 7.15, y3 = 7.45, x4 = 7.10, y4 = 3.25;
		double center2X = 1.15, center2Y = 1.10;
		double radius2 = 5;

		double ipx3 = 0.0, ipy3 = 0.0, ipx4 = 0.0, ipy4 = 0.0;
		REQUIRE(cross_segment_to_circle({x3, y3}, {x4, y4}, {center2X, center2Y}, radius2, ipx3, ipy3, ipx4, ipy4) == 0);
		REQUIRE((ipx3 - 0.0) < 0.0001);
		REQUIRE((ipy3 - 0.0) < 0.0001);
		REQUIRE((ipx4 - 0.0) < 0.0001);
		REQUIRE((ipy4 - 0.0) < 0.0001);

		double x5 = 5.15, y5 = 1.15, x6 = 7.10, y6 = 6.25;
		double center3X = 6.15, center3Y = 6.10;
		double radius3 = 3;

		double ipx5 = 0.0, ipy5 = 0.0, ipx6 = 0.0, ipy6 = 0.0;
		REQUIRE(cross_segment_to_circle({x5, y5}, {x6, y6}, {center3X, center3Y}, radius3, ipx5, ipy5, ipx6, ipy6) == 1);
		REQUIRE((ipx5 - 7.10) < 0.0001);
		REQUIRE((ipy5 - 6.25) < 0.0001);
		REQUIRE((ipx6 - 0.0) < 0.0001);
		REQUIRE((ipy6 - 0.0) < 0.0001);
	}
	SECTION("add_line_breaks(string, size_t, bool")
	{
		using namespace algo;
		using namespace strings;

		std::string text("text in the line text in the line text in the line");
		std::string result = add_line_breaks(text, 18);
		REQUIRE("text in the line\n"
		        "text in the line\n"
		        "text in the line" == result);

		std::string text2("this text a very longest string - 'bla blablabla' and 'blabla'");
		std::string result2 = add_line_breaks(text2, 5);
		REQUIRE("this\n"
		        "text\n"
		        "a\n"
		        "very\n"
		        "longest\n"
		        "string\n"
		        "-\n"
		        "'bla\n"
		        "blablabla'\n"
		        "and\n"
		        "'blabla'" == result2);

		std::string text3("lalala lalala");
		std::string result3 = add_line_breaks(text3, 13);
		REQUIRE("lalala lalala" == result3);

		std::string text4("text in line one\n"
		                  "is line two\n"
		                  "and\n"
		                  "la lala three");
		std::string result4 = add_line_breaks(text4, 8);
		REQUIRE("text in\n"
		        "line\n"
		        "one\n"
		        "is line\n"
		        "two\n"
		        "and\n"
		        "la lala\n"
		        "three" == result4);

		std::string text5("line\n"
		                  "line\n"
		                  "line and line\n"
		                  "line again\n"
		                  "last line");
		std::string result5 = add_line_breaks(text5, 14);
		REQUIRE("line\n"
		        "line\n"
		        "line and line\n"
		        "line again\n"
		        "last line" == result5);

		std::string text6("this text a very longest string - 'bla blablabla' and 'blabla'");
		std::string result6 = add_line_breaks(text6, 5, true);
		REQUIRE("this\n"
		        " text\n"
		        " a\n"
		        " very\n"
		        " longest\n"
		        " string\n"
		        " -\n"
		        " 'bla\n"
		        " blablabla'\n"
		        " and\n"
		        " 'blabla'" == result6);
	}
	SECTION("signal<void>")
	{
		using namespace algo;
		using namespace notice;

		signal<> sig;
		bool bl = false;

		sig.connect([&bl]() {
			bl = true;
		});
		sig.emit();
		REQUIRE(bl);
	}
	SECTION("signal<int, int, char>")
	{
		using namespace algo;
		using namespace notice;

		signal<int, int, char> sig;
		int value1, value2;
		char ch;

		sig.connect([&value1, &value2, &ch](int one, int two, char c) {
			value1 = one, value2 = two;
			ch = c;
		});
		sig.emit(10, 20, 'g');
		REQUIRE(value1 == 10);
		REQUIRE(value2 == 20);
		REQUIRE(ch == 'g');
	}
	SECTION("signal<int, double>::connect(Class, Args ...)")
	{
		class test
		{
		public:
			test() = default;
			void check(int i, double d)
			{
				value1 = i;
				value2 = d;
			}

			int value1 = 0;
			double value2 = 0;
		};

		using namespace algo;
		using namespace notice;

		signal<int, double> sig;
		test t;

		sig.connect(&t, &test::check);
		sig.emit(10, 20.2);
		REQUIRE(t.value1 == 10);
		REQUIRE(t.value2 == 20.2);
		sig.disconnect(&t, &test::check);
		sig.emit(0, 0.0);
		REQUIRE(t.value1 == 10);
		REQUIRE(t.value2 == 20.2);
	}
	SECTION("signal<Args>::connect(Class, Args ...)")
	{
		class test
		{
		public:
			test() = default;
			void check(std::string& s)
			{
				value = s;
			}

			std::string value;
		};

		class test2
		{
		public:
			test2() = default;
			void check(const std::string& t)
			{
				value = t;
			}

			std::string value;
		};
		using namespace algo;
		using namespace notice;

		signal<std::string> sig;
		test t;
		test2 t2;

		sig.connect(&t, &test::check);
		sig.connect(&t2, &test2::check);
		std::string ss = "value string";
		sig.emit(ss);
		REQUIRE(t.value == "value string");
		REQUIRE(t2.value == "value string");
		sig.disconnect_all();
		ss = "string value";
		sig.emit(ss);
		REQUIRE(t.value == "value string");
		REQUIRE(t2.value == "value string");
	}
}
